using AirMaid.Core.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

public class PricingController: Controller
{  
    private readonly IOptions<ItemPricing> pricing;

    public PricingController(IOptions<ItemPricing> _pricing)
    {
        pricing = _pricing;
    }
 
    
        [HttpGet("api/pricing")]
    public IActionResult Pricing()         
    {

        return Json( pricing.Value);
    }
}