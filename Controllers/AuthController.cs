using System.Threading.Tasks;
using AirMaid;
using AirMaid.Core.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;



namespace airmaid.Controllers
{
    public class AuthController:Controller
    {
        private readonly AirMaidDBContext context;
        private readonly SignInManager<User> signInMgr;
        private readonly UserManager<User> userManager;
        public AuthController(AirMaidDBContext context, SignInManager<User> signInMgr, UserManager<User> userManager)
        {
            this.userManager = userManager;
            this.signInMgr = signInMgr;
            this.context = context;

        }



        [HttpPost("api/auth/login")]
        public async Task<IActionResult> Login([FromBody]CredentialModel model)
        {
            try
            {

                System.Console.WriteLine("OK****");
                var user = await userManager.FindByEmailAsync(model.Email);
                var result = await signInMgr.PasswordSignInAsync(user.UserName, model.Password, false, false);
                
                if (result.Succeeded)
                {
                     return Ok( new {email=user.Email,password=model.Password,username=user.UserName});
                }

            }
            catch (System.Exception ex)
            {
                // logger.LogError("$exception thrown while loggin in:{ex}");
            }
            return new BadRequestResult();
        }




        [HttpGet("api/auth/logout")]
        public async Task<IActionResult> Logout()
        {
            try
            {
                await signInMgr.SignOutAsync();

                return Ok();
            }
            catch (System.Exception ex)
            {
                // logger.LogError("$exception thrown while loggin in:{ex}");
            }
            return new BadRequestResult();
        }
    }
}