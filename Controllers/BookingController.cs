using System;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using AirMaid.Core.Models;
using AirMaid.Core;
using System.Collections.Generic;
using AirMaid.Controllers.Resources;
using Microsoft.AspNetCore.Authorization;

namespace AirMaid.Controllers
{
    [Route("/api/bookings")]
    public class BookingController : Controller
    {
        private readonly IMapper mapper;
        private readonly IVehicleRepository repository;
        private readonly IUnitOfWork unitOfWork;
   private readonly AirMaidDBContext context;
        public BookingController(IMapper mapper, IVehicleRepository repository, IUnitOfWork unitOfWork,AirMaidDBContext context)
        {
            this.unitOfWork = unitOfWork;
            this.repository = repository;
            this.mapper = mapper;
                this.context = context;
        }

  
        [HttpPost]
        public async Task<IActionResult> CreateVehicle([FromBody] Booking vehicleResource)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

          //  var vehicle = mapper.Map<SaveVehicleResource, Vehicle>(vehicleResource);
            //vehicle.LastUpdate = DateTime.Now;

           // repository.Add(vehicle);
           // await unitOfWork.CompleteAsync();

            //vehicle = await repository.GetVehicle(vehicle.Id);

            //var result = mapper.Map<Vehicle, VehicleResource>(vehicle);
            vehicleResource.Id=100;

            return Ok(vehicleResource);
        }

      
        [Authorize]      
       [HttpGet]
      
        public async Task<IEnumerable<KeyValuePairResource>> GetBookings()
        {
            var features = await context.Features.ToListAsync();
            return mapper.Map<List<Feature>, List<KeyValuePairResource>>(features);
            
        }

      



      

    }
}