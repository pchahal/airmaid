﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace AirMaid.Migrations
{
    public partial class SeedVehicles : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("Insert INTO Vehicles (ModelId,IsRegistered,ContactName,ContactEmail,ContactPhone,LastUpdate) VALUES (1,0,'contact1','email1','phone1','jan 15 2015')");
            migrationBuilder.Sql("Insert INTO Vehicles (ModelId,IsRegistered,ContactName,ContactEmail,ContactPhone,LastUpdate) VALUES (2,0,'contact2','email2','phone2','jan 15 2015')");
            migrationBuilder.Sql("Insert INTO Vehicles (ModelId,IsRegistered,ContactName,ContactEmail,ContactPhone,LastUpdate) VALUES (3,0,'contact3','email3','phone3','jan 15 2015')");
            migrationBuilder.Sql("Insert INTO Vehicles (ModelId,IsRegistered,ContactName,ContactEmail,ContactPhone,LastUpdate) VALUES (1,0,'contact4','email4','phone4','jan 15 2015')");
            migrationBuilder.Sql("Insert INTO Vehicles (ModelId,IsRegistered,ContactName,ContactEmail,ContactPhone,LastUpdate) VALUES (2,0,'contact5','email5','phone5','jan 15 2015')");
            migrationBuilder.Sql("Insert INTO Vehicles (ModelId,IsRegistered,ContactName,ContactEmail,ContactPhone,LastUpdate) VALUES (3,0,'contact6','email6','phone6','jan 15 2015')");
            migrationBuilder.Sql("Insert INTO Vehicles (ModelId,IsRegistered,ContactName,ContactEmail,ContactPhone,LastUpdate) VALUES (1,0,'contact7','email7','phone7','jan 15 2015')");
            migrationBuilder.Sql("Insert INTO Vehicles (ModelId,IsRegistered,ContactName,ContactEmail,ContactPhone,LastUpdate) VALUES (2,0,'contact8','email8','phone8','jan 15 2015')");
            migrationBuilder.Sql("Insert INTO Vehicles (ModelId,IsRegistered,ContactName,ContactEmail,ContactPhone,LastUpdate) VALUES (3,0,'contact9','email9','phone9','jan 15 2015')");
            migrationBuilder.Sql("Insert INTO Vehicles (ModelId,IsRegistered,ContactName,ContactEmail,ContactPhone,LastUpdate) VALUES (1,0,'contact10','email10','phone10','jan 15 2015')");

        }
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("DELETE FROM Vehicles WHERE ContactEmail IN ('email1', 'email2', 'email3','email4','email5','email6','email7','email8','email9','email10')");
        }
    }
}
