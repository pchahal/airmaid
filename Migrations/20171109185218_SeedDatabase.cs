﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace AirMaid.Migrations
{
    public partial class SeedDatabase : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("Insert INTO Makes (Name) VALUES ('Make1')");
            migrationBuilder.Sql("Insert INTO Makes (Name) VALUES ('Make2')");
            migrationBuilder.Sql("Insert INTO Makes (Name) VALUES ('Make3')");
            migrationBuilder.Sql("Insert INTO Models (Name,MakeID) VALUES ('Make1-ModelA',(SELECT ID FROM Makes WHERE Name = 'Make1'))");
            migrationBuilder.Sql("Insert INTO Models (Name,MakeID) VALUES ('Make1-ModelB',(SELECT ID FROM Makes WHERE Name = 'Make1'))");
            migrationBuilder.Sql("Insert INTO Models (Name,MakeID) VALUES ('Make1-ModelC',(SELECT ID FROM Makes WHERE Name = 'Make1'))");

            migrationBuilder.Sql("Insert INTO Models (Name,MakeID) VALUES ('Make2-ModelC',(SELECT ID FROM Makes WHERE Name = 'Make2'))");
            migrationBuilder.Sql("Insert INTO Models (Name,MakeID) VALUES ('Make2-ModelC',(SELECT ID FROM Makes WHERE Name = 'Make2'))");
            migrationBuilder.Sql("Insert INTO Models (Name,MakeID) VALUES ('Make2-ModelC',(SELECT ID FROM Makes WHERE Name = 'Make3'))");

            migrationBuilder.Sql("Insert INTO Models (Name,MakeID) VALUES ('Make3-ModelC',(SELECT ID FROM Makes WHERE Name = 'Make3'))");
            migrationBuilder.Sql("Insert INTO Models (Name,MakeID) VALUES ('Make3-ModelC',(SELECT ID FROM Makes WHERE Name = 'Make3'))");
            migrationBuilder.Sql("Insert INTO Models (Name,MakeID) VALUES ('Make3-ModelC',(SELECT ID FROM Makes WHERE Name = 'Make3'))");

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("DELETE FROM  Makes WHERE Name IN ('Make1','Make2','Make3')");

        }
    }
}
