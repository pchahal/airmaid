using Microsoft.EntityFrameworkCore;
using AirMaid.Core.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace AirMaid
{
    public class AirMaidDBContext : IdentityDbContext<User>
    {

        public DbSet<Make> Makes { get; set; }
        public DbSet<Feature> Features { get; set; }
        public DbSet<Vehicle> Vehicles { get; set; }

        public DbSet<Model> Models { get; set; }

        public DbSet<Photo> Photos { get; set; }

        public AirMaidDBContext(DbContextOptions<AirMaidDBContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder); 
            modelBuilder.Entity<VehicleFeature>().HasKey(vf => new { vf.VehicleId, vf.FeatureId });
        }
    }
}

