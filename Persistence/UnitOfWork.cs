using System.Threading.Tasks;

namespace AirMaid.Core
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly AirMaidDBContext context;

        public UnitOfWork(AirMaidDBContext context)
        {
            this.context = context;
        }

        public async Task CompleteAsync()
        {
            await context.SaveChangesAsync();
        }
    }
}