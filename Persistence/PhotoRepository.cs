using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using AirMaid.Core;
using AirMaid.Core.Models;

namespace AirMaid.Persistence
{
  public class PhotoRepository : IPhotoRepository
  {
    private readonly AirMaidDBContext context;
    public PhotoRepository(AirMaidDBContext context)
    {
      this.context = context;
    }
    public async Task<IEnumerable<Photo>> GetPhotos(int vehicleId)
    {
      return await context.Photos
        .Where(p => p.VehicleId == vehicleId)
        .ToListAsync();
    }
  }
}