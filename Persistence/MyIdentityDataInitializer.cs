using Microsoft.AspNetCore.Identity;

namespace AirMaid.Persistence
{
    public static class MyIdentityDataInitializer
    {
      public static void SeedData(UserManager<User> userManager, RoleManager<IdentityRole> roleManager)
{
    SeedRoles(roleManager);
   SeedUsers(userManager);
}


        public static void SeedRoles(RoleManager<IdentityRole> roleManager)
        {
            if (!roleManager.RoleExistsAsync("NormalUser").Result)
            {
                IdentityRole role = new IdentityRole();
                role.Name = "NormalUser";
                IdentityResult roleResult = roleManager.
                CreateAsync(role).Result;
            }


            if (!roleManager.RoleExistsAsync("Administrator").Result)
            {
                IdentityRole role = new IdentityRole();
                role.Name = "Administrator";
                IdentityResult roleResult = roleManager.
                CreateAsync(role).Result;
            }
        }


        public static void SeedUsers(UserManager<User> userManager)
        {
            if (userManager.FindByNameAsync("user1").Result == null)
            {
                User user = new User();
                user.UserName = "user1";
                user.Email = "user1@localhost";
                IdentityResult result = userManager.CreateAsync(user, "Password1!").Result;

                if (result.Succeeded)
                {
                    userManager.AddToRoleAsync(user, "NormalUser").Wait();
                }
            }


            if (userManager.FindByNameAsync("admin").Result == null)
            {
                User user = new User();
                user.UserName = "admin";
                user.Email = "admin@localhost";
                IdentityResult result = userManager.CreateAsync(user, "Password1!").Result;

                if (result.Succeeded)
                {
                    userManager.AddToRoleAsync(user, "Administrator").Wait();
                }
            }
        }

    }







}