import {IMyDate} from 'mydatepicker';

export class Booking {
    
      constructor(
        public postal:string,
        public beds:number=1,
        public baths:number=1,
        public selDate: IMyDate,
        public time:string,
        public email:string,


        public weeklySchedule:number,
        public monthlyTerm:number,
        public firstName:string,
        public lastName:string,
        public address:string,
        public apartment:string,
        public city:string,
        public province:string,
        public phone:string,
        public referral:number,
        public promoCode:number,

        public insideCabinets:boolean,
        public insideFridge:boolean,
        public carpetCleaning:boolean,
        public laundry:boolean,
        public windows:boolean

      ) {  }
    
    }