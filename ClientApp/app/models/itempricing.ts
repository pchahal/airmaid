

export class ItemPricing {
    
      constructor(
         
        public monthlyPlan:[{price:number,discount:number},{price:number,discount:number},{price:number,discount:number}],
        public biweeklyPlan:[{price:number,discount:number},{price:number,discount:number},{price:number,discount:number}],
        public weeklyPlan:[{price:number,discount:number},{price:number,discount:number},{price:number,discount:number}],
        public onetimePlan:[{price:number,discount:number}],
        
        public coupon:number,
        public supportFee:number,
        public insideCabinets:number,
        public insideFridge:number,
        public carpetCleaning:number,
        public laundry:number,
        public windows:number,
      ) {  }
    }
