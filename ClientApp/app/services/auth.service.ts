import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { User } from './../models/user';

@Injectable()
export class AuthService {

  constructor(private http: Http) {   
   }
   private user:User={email:"",password:"",username:""};
   
  

  

  login(user:User){
    this.user=user;
   return this.http.post('/api/auth/login',user)
      .map(res => res.json());
  }


  logout(){
    this.user.email="";this.user.password="";this.user.username="";  
    
   return this.http.get('/api/auth/logout')
      .map(res => res.json());
  }

  getLoggedInUser()
  {
      return this.user;
  }
  setLoggedInUser(user:User)
  {
    this.user=user;
  }

}






