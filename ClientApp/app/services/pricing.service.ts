import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Booking } from './../models/booking';
import { ItemPricing } from '../models/itempricing';

@Injectable()
export class PricingService {

  constructor(
    private http: Http
  ) {
   this.getMockBooking();
   
   }

  private bookingData:any;

  setBookingData(data:any){
    this.bookingData = data;
  }

  getBookingData(){
    let temp = this.bookingData;
    this.clearData();
    return temp;
  }

  clearData(){
    this.bookingData = undefined;
  }



  getMockBooking()
  {
    let booking: Booking  = {
      postal:'',
      beds:2,
      baths:2,
      selDate:  {year: 0, month: 0, day: 0},
      time:'2:00 PM',
      email:'',
       weeklySchedule:1,
       monthlyTerm:1,
       firstName:'',
       lastName:'',
       address:'',
       apartment:'',
       city:'',
       province:'BC',
       phone:'',
       referral:0,
       promoCode:0,
  
       insideCabinets:false,
       insideFridge:false,
       carpetCleaning:false,
       laundry:false,
       windows:false
      };
  
      this.bookingData = booking;
  }


  getPricingData(beds:number,baths:number){
    return this.http.get('/api/pricing')
      .map(res => res.json());
  }

}






