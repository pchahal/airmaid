import { Component, OnInit } from '@angular/core';
import { AuthService } from './../../services/auth.service';
import {User } from './../../models/user';
import { Router } from '@angular/router';



@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {


  public user:User;
  
  constructor( private authService: AuthService,    private router: Router  ) {
    
   }

  ngOnInit() {
    this.user= this.authService.getLoggedInUser();
  }

  onLogout()
  {
    this.authService.logout().subscribe();
  }



   get isLoggedIn()
  {
   // return true; 
    if (this.user.username=="")
    return false;
    else
    return true;
    
  }

  
}
