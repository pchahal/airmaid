import { Component, OnInit,ViewChild } from '@angular/core';
import {Booking } from './../../models/booking';
import { PricingService } from './../../services/pricing.service';
import { Router } from '@angular/router';
import {MyDatePicker,IMyDpOptions, IMyDateModel,IMySelector,IMyDate} from 'mydatepicker';

@Component({
  selector: 'app-pricing-form',
  templateUrl: './pricing-form.component.html',
  styleUrls: ['./pricing-form.component.css']
})
export class PricingFormComponent implements OnInit {

  constructor(    private pricingService: PricingService,    private router: Router  ) { }

  ngOnInit() {
      
    let d: Date = new Date();
    this.booking.selDate = {year: d.getFullYear(), 
                    month: d.getMonth() + 1, 
                    day: d.getDate()};
  

  }
  myDatePickerOptions: IMyDpOptions = {
    dateFormat: 'dd.mm.yyyy',
    showClearDateBtn:false
};
@ViewChild('mydp') mydp: MyDatePicker;
  booking: Booking  = {
    postal:'',
    beds:1,
    baths:1,
    selDate:  {year: 0, month: 0, day: 0},
    time:'2:00 PM',
    email:'',
     weeklySchedule:1,
     monthlyTerm:1,
     firstName:'',
     lastName:'',
     address:'',
     apartment:'',
     city:'',
     province:'BC',
     phone:'',
     referral:0,
     promoCode:0,

     insideCabinets:false,
     insideFridge:false,
     carpetCleaning:false,
     laundry:false,
     windows:false
    };

   submitted = false;

   onSubmit() { this.submitted = true;
    this.pricingService.setBookingData(this.booking);
    this.router.navigate(['/booking']);
    

  }

  public selector: IMySelector = {
    open: false
};

onDateChanged(event: IMyDateModel) {
  // Update value of selDate variable
  this.booking.selDate = event.date;
}


   OnBeds(beds:number)
   {
     this.booking.beds = beds;
   }
   OnBaths(baths:number)
   {
     this.booking.baths = baths;
   }
   OnTime(time:string)
   {
     this.booking.time = time;
   }


  get diagnostic() { return JSON.stringify(this.booking); 
  
  }
  get beds() { return this.booking.beds + ' beds'; }
  get baths() { return this.booking.baths + ' baths'; }
  get time() { return this.booking.time; }
  
}
