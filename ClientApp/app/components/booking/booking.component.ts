import { Component, OnInit } from '@angular/core';
import {Booking } from './../../models/booking';
import { PricingService } from './../../services/pricing.service';
import { ItemPricing } from '../../models/itempricing';
import { Observable } from 'rxjs/Observable';


@Component({
  selector: 'app-booking',
  templateUrl: './booking.component.html',
  styleUrls: ['./booking.component.css']
})
export class BookingComponent implements OnInit {

  public booking:Booking;
  public pricing:ItemPricing;
  constructor( private pricingService: PricingService) {
    this.booking=this.pricingService.getBookingData();
   }

  ngOnInit() {
    
    this.pricing = {
      monthlyPlan:[{price:168,discount:0},{price:90,discount:46},{price:87,discount:48}],
      biweeklyPlan:[{price:90,discount:0},{price:87,discount:3},{price:81,discount:10}],
      weeklyPlan:[{price:87,discount:0},{price:81,discount:6},{price:75,discount:13}],
      onetimePlan:[{price:200,discount:0}],
      coupon:30,
      supportFee:3,
      insideCabinets:12.50,
      insideFridge:12.50,
      carpetCleaning:12.50,
      laundry:25,
      windows:12.50,
    };

    this.pricingService.getPricingData(this.booking.beds,this.booking.baths).subscribe(pricing => this.pricing=pricing);
  }


  AddExtra(extra:number)
  {

    if (extra==0)
    {
      if (this.booking.insideCabinets==false)
      this.booking.insideCabinets=true;
      else 
      this.booking.insideCabinets=false;
    }
    if (extra==1)
    {
      if (this.booking.insideFridge==false)
      this.booking.insideFridge=true;
      else 
      this.booking.insideFridge=false;
    }
    if (extra==2)
    {
      if (this.booking.carpetCleaning==false)
      this.booking.carpetCleaning=true;
      else 
      this.booking.carpetCleaning=false;
    }
    if (extra==3)
    {
      if (this.booking.laundry==false)
      this.booking.laundry=true;
      else 
      this.booking.laundry=false;
    }
    if (extra==4)
    {
      if (this.booking.windows==false)
      this.booking.windows=true;
      else 
      this.booking.windows=false;
    }
  }
  
  //*****pricing getters  */
get cleaningPlan()
{
  if (this.booking.weeklySchedule==0)
     return "Monthly";
    else if (this.booking.weeklySchedule==1)
     return "BiWeekly";
     else if (this.booking.weeklySchedule==2) 
    return "Weekly";
    else
    return "One Time";
}

get minimumTerm()
{
  if (this.booking.monthlyTerm==0)
  return "3";
 else if (this.booking.monthlyTerm==1)
  return "6";
 else 
 return "12";
}

get dateTime()
{
  let time:string = " @ " + this.booking.time;

  let date:Date = new Date(this.booking.selDate.year,this.booking.selDate.month,this.booking.selDate.day);
  return date.toDateString() + time;
}

get Price3Month()
{
  if (this.booking.weeklySchedule==0)//monthlyTerm
    return this.pricing.monthlyPlan[0].price;
    if (this.booking.weeklySchedule==1)//biweeklyTerm
    return this.pricing.biweeklyPlan[0].price;
    if (this.booking.weeklySchedule==2)//weeklyTerm
    return this.pricing.weeklyPlan[0].price;
}
get Price6Month()
{
  if (this.booking.weeklySchedule==0)//monthlyTerm
    return this.pricing.monthlyPlan[1].price;
    if (this.booking.weeklySchedule==1)//biweeklyTerm
    return this.pricing.biweeklyPlan[1].price;
    if (this.booking.weeklySchedule==2)//weeklyTerm
    return this.pricing.weeklyPlan[1].price;
}
get Price12Month()
{
  if (this.booking.weeklySchedule==0)//monthlyTerm
    return this.pricing.monthlyPlan[2].price;
    if (this.booking.weeklySchedule==1)//biweeklyTerm
    return this.pricing.biweeklyPlan[2].price;
    if (this.booking.weeklySchedule==2)//weeklyTerm
    return this.pricing.weeklyPlan[2].price;
}

get Savings6Month()
{
  if (this.booking.weeklySchedule==0)//monthlyTerm
  return this.pricing.monthlyPlan[1].discount;
  if (this.booking.weeklySchedule==1)//biweeklyTerm
  return this.pricing.biweeklyPlan[1].discount;
  if (this.booking.weeklySchedule==2)//weeklyTerm
  return this.pricing.weeklyPlan[1].discount;
}
get Savings12Month()
{
  if (this.booking.weeklySchedule==0)//monthlyTerm
  return this.pricing.monthlyPlan[2].discount;
  if (this.booking.weeklySchedule==1)//biweeklyTerm
  return this.pricing.biweeklyPlan[2].discount;
  if (this.booking.weeklySchedule==2)//weeklyTerm
  return this.pricing.weeklyPlan[2].discount;
}
get Coupon()
{
  let savings=this.PricePerCleaning*(this.pricing.coupon/100);
  return savings;
}

get PricePerCleaning()
{

  let extras=0;
  let cleaningPrice=0;
  if (this.booking.insideCabinets==true)
      extras+=this.pricing.insideCabinets;
      if (this.booking.insideFridge==true)
      extras+=this.pricing.insideFridge;
      if (this.booking.carpetCleaning==true)
      extras+=this.pricing.carpetCleaning;
      if (this.booking.laundry==true)
      extras+=this.pricing.laundry;
      if (this.booking.windows==true)
      extras+=this.pricing.windows;
  
    
  if (this.booking.weeklySchedule==0)
    {
      if (this.booking.monthlyTerm==0)
      cleaningPrice= this.pricing.monthlyPlan[0].price;
      else if (this.booking.monthlyTerm==1)
      cleaningPrice= this.pricing.monthlyPlan[1].price;
      if (this.booking.monthlyTerm==2)
      cleaningPrice= this.pricing.monthlyPlan[2].price;     
    }
   else if (this.booking.weeklySchedule==1)
    {
      if (this.booking.monthlyTerm==0)
      cleaningPrice= this.pricing.biweeklyPlan[0].price;
      else if (this.booking.monthlyTerm==1)
      cleaningPrice= this.pricing.biweeklyPlan[1].price;
      if (this.booking.monthlyTerm==2)
      cleaningPrice= this.pricing.biweeklyPlan[2].price;     
    }
    else if (this.booking.weeklySchedule==2)
    {
      if (this.booking.monthlyTerm==0)
      cleaningPrice= this.pricing.weeklyPlan[0].price;
      else if (this.booking.monthlyTerm==1)
      cleaningPrice= this.pricing.weeklyPlan[1].price;
      if (this.booking.monthlyTerm==2)
      cleaningPrice= this.pricing.weeklyPlan[2].price;     
    }
    else if (this.booking.weeklySchedule==3)
    {
      cleaningPrice= this.pricing.onetimePlan[0].price;
    }
    return cleaningPrice+extras;
}

get Total()
{
  return this.PricePerCleaning-this.Coupon+ this.pricing.supportFee;
}











  
  get diagnostic() { 
   let booking:string= JSON.stringify(this.booking);
   let pricing:string= JSON.stringify(this.pricing);
   return booking+ "                                " + pricing; 
  }
}
