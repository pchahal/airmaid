import { Component, OnInit } from '@angular/core';
import {User } from './../../models/user';
import { AuthService } from './../../services/auth.service';
import { Router } from '@angular/router';



@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public user:User={email:"",password:"",username:""};

  constructor(  private authService: AuthService,    private router: Router  ) { }

  ngOnInit() {
  }

  onSubmit() { 
   
    
    this.authService.login(this.user).subscribe( user =>
       {
         this.user=user;    
         this.authService.setLoggedInUser(user);
                  this.router.navigate(['/home']);
        }
  );
    
    
  }

  onLogout() { 
    
    this.authService.logout().subscribe(user => this.user.username="");
  }



    

}
