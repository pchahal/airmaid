using System.Collections.Generic;
using System.Threading.Tasks;
using AirMaid.Core.Models;

namespace AirMaid.Core
{
    public interface IPhotoRepository
    {
         Task<IEnumerable<Photo>> GetPhotos(int vehicleId);
    }
}