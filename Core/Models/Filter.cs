namespace AirMaid.Core.Models
{
    public class Filter
    {
        public int? MakeId { get; set; }
        public int? ModelId { get; set; }
    }
}