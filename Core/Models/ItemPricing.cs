using System.Collections.Generic;

namespace AirMaid.Core.Models
{
    public class PriceDiscountPair
    {
        public decimal price {get;set;}
        public decimal discount { get; set; }
    }
    public class ItemPricing
    {
        public List<PriceDiscountPair> monthlyPlan{get;set;}
        public List<PriceDiscountPair> biweeklyPlan{get;set;}
        public List<PriceDiscountPair> weeklyPlan{get;set;}
        public List<PriceDiscountPair> onetimePlan{get;set;}

        public decimal coupon{ get; set; }
        public decimal supportFee{ get; set; }
        public decimal insideCabinets{ get; set; }
        public decimal insideFridge{ get; set; }
        public decimal carpetCleaning{ get; set; }
        public decimal laundry{ get; set; }
        public decimal windows{ get; set; }
    }
}



  