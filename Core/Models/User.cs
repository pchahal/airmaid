using Microsoft.AspNetCore.Identity;

namespace AirMaid
{
    public class User : IdentityUser
    {
        public string postal { get; set; }
        public string address { get; set; }
        public string apartment { get; set; }
        public string city { get; set; }
        public string province { get; set; }
    }

    
}