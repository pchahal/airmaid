using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System;

namespace AirMaid.Core.Models
{
    public class Booking
    {
        public int Id { get; set; }
        [Required]
        [StringLength(255)]
        public string postal { get; set; }
        [Required]
        [Range(1, 10)]
        public byte beds { get; set; }
        [Required]
        [Range(1, 10)]
        public byte baths { get; set; }

        [Required]
        public DateTime selDate { get; set; }
        [Required]
        public string email { get; set; }

        [Required]
        [Range(0, 3)]
        public byte weeklySchedule { get; set; }
        [Required]
        [Range(0, 2)]
        public byte monthlyTerm { get; set; }
        [Required]
        public string firstName { get; set; }
        [Required]
        public string lastName { get; set; }
        [Required]
        public string address { get; set; }
        public string apartment { get; set; }
        [Required]
        public string city { get; set; }
        [Required]
        public string province { get; set; }
        [Required]
        public string phone { get; set; }
        public string referral { get; set; }
        public byte promoCode { get; set; }
        public bool insideCabinets { get; set; }
        public bool insideFridge { get; set; }
        public bool carpetCleaning { get; set; }
        public bool laundry { get; set; }
        public bool windows { get; set; }



        public string confirmationCode { get; set; }
        public DateTime LastUpdate { get; set; }
        public byte Status { get; set; } //pending|confirmed|complete|cancelled,
        public int CleanerId { get; set; }
    }
}


