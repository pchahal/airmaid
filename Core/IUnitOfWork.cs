using System;
using System.Threading.Tasks;

namespace AirMaid.Core
{
    public interface IUnitOfWork
    {
        Task CompleteAsync();
    }
}